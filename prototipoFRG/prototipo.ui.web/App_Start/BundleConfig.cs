﻿using System.Web;
using System.Web.Optimization;

namespace prototipo.ui.web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css"));

            // Template

            bundles.Add(new StyleBundle("~/template-content/css").Include(
                "~/template-content/css/customize-template.css",
                "~/Content/Site.css"));

            bundles.Add(new ScriptBundle("~/template-content/js").Include(
                "~/template-content/js/bootstrap/bootstrap-transition.js",
                "~/template-content/js/bootstrap/bootstrap-alert.js",
                "~/template-content/js/bootstrap/bootstrap-modal.js",
                "~/template-content/js/bootstrap/bootstrap-dropdown.js",
                "~/template-content/js/bootstrap/bootstrap-scrollspy.js",
                "~/template-content/js/bootstrap/bootstrap-tab.js",
                "~/template-content/js/bootstrap/bootstrap-tooltip.js",
                "~/template-content/js/bootstrap/bootstrap-popover.js",
                "~/template-content/js/bootstrap/bootstrap-button.js",
                "~/template-content/js/bootstrap/bootstrap-collapse.js",
                "~/template-content/js/bootstrap/bootstrap-carousel.js",
                "~/template-content/js/bootstrap/bootstrap-typeahead.js",
                "~/template-content/js/bootstrap/bootstrap-affix.js",
                "~/template-content/js/bootstrap/bootstrap-datepicker.js",
                "~/template-content/js/jquery/virtual-tour.js",
                "~/template-content/js/jquery/jquery-tablesorter.js",
                "~/template-content/js/jquery/jquery-chosen.js"));

            // AngularJS

            bundles.Add(new ScriptBundle("~/angularjs").Include(
                "~/Scripts/angular.min.js",
                "~/Scripts/app/app.js"));

            // XEditable

            bundles.Add(new StyleBundle("~/xeditable/css").Include(
                "~/Content/xeditable.css"));

            bundles.Add(new ScriptBundle("~/xeditable/js").Include(
                "~/Scripts/xeditable/xeditable.js",
                "~/Scripts/xeditable/xeditable.min.js"));

            //JQuery Chosen
            
            bundles.Add(new StyleBundle("~/Content/jquery-chosen").Include(
                "~/Content/chosen.css",
                "~/Content/chosen.min.css"));
        }
    }
}
