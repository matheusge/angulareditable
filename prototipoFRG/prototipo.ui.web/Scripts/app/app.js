﻿var app = angular.module('app', ['xeditable', 'localytics.directives']);

app.run(function (editableOptions) {
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});

app.controller('EditableRowCtrl', function ($scope, $filter, $http) {

    $scope.contas = [
      { id: 1, descricao: 'Folha de pagamento', valor: '2.500,00', categoria: 4 },
      { id: 2, descricao: 'Conta energia', valor: '140', categoria: 3 },
      { id: 3, descricao: 'Compra mercadorias', valor: '200', categoria: 1 }
    ];

    $scope.categorias = [
      { value: 1, text: ''},
      { value: 2, text: 'Receitas' },
      { value: 3, text: 'Despesas Fixas' },
      { value: 4, text: 'Despesas Variáveis' },
      { value: 5, text: 'Impostos' }
    ];

    //$scope.groups = [];
    //$scope.loadGroups = function () {
    //    return $scope.groups.length ? null : $http.get('/groups').success(function (data) {
    //        $scope.groups = data;
    //    });
    //};

    //$scope.showGroup = function (user) {
    //    if (user.group && $scope.groups.length) {
    //        var selected = $filter('filter')($scope.groups, { id: user.group });
    //        return selected.length ? selected[0].text : 'Not set';
    //    } else {
    //        return user.groupName || 'Not set';
    //    }
    //};

    $scope.showCategorias = function (conta) {
        var selected = [];
        if (conta.categoria) {
            selected = $filter('filter')($scope.categorias, { value: conta.categoria });
        }
        return selected.length ? selected[0].text : 'Not set';
    };

    $scope.checkDescricao = function (data, id) {
        if (!data || 0 === data.length) {
            return "Necessário informar uma descrição para a conta.";
        }
    };

    $scope.checkValor = function (data, id) {
        if (!data || 0 === data.length || data.value > 0) {
            return "Necessário informar um valor válido.";
        }
    }

    $scope.saveUser = function (data, id) {
        //$scope.user not updated yet
        angular.extend(data, { id: id });
        //return $http.post('/saveUser', data);
        return true;
    };

    // remove user
    $scope.removeUser = function (index) {
        $scope.contas.splice(index, 1);
    };

    // add user
    $scope.addUser = function () {
        $scope.inserted = {
            id: $scope.contas.length + 1,
            name: '',
            status: null,
            group: null
        };
        $scope.contas.push($scope.inserted);
    };
});

//$scope.show = function () {
//    ModalService.showModal({
//        templateUrl: 'modal.html',
//        controller: "ModalController"
//    }).then(function (modal) {
//        modal.element.modal();
//        modal.close.then(function (result) {
//            $scope.message = "You said " + result;
//        });
//    });
//};

app.controller('ModalController', function ($scope, close) {

    $scope.close = function (result) {
        close(result, 500); // close, but give 500ms for bootstrap to animate
    };

});