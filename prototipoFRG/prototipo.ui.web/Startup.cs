﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(prototipo.ui.web.Startup))]
namespace prototipo.ui.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
